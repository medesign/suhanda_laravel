@extends('master')

@section('judul')
Halaman Index
@endsection

@section('content')
<!-- bagian awal, isinya tag heading -->
<div>
  <h1>Media Online</h1>
</div>

<div>
  <h2>Sosial Media Developer</h2>
</div>
<!-- konten -->
<div>
  <p>Belajar dan berbagi agar hidup menjadi lebih baik</p>
</div>
<!-- heading -->
<div>
  <h2>Benefit Join di Media Online</h2>
</div>
<!-- konten list pada html -->
<div>
  <ul>
      <li>Mendapatkan motivasi dari sesama para Developer</li>
      <li>Sharing knowledge</li>
      <li>Dibuat oleh calon web developer terbaik</li>
    </ul>
</div>
<div>
    <!-- heading -->
    <h2>Cara Bergabung ke Media Online</h2>
  </div>
  <!-- konten list pada html -->
<div>
    <ol>
    <li>Mengunjungi Website ini</li>
    <li>Mendaftarkan di <a href="/register">Form Sign Up</a></li>
    <li>Selesai</li>
  </div>
  <p><em>&COPY;Suhanda-PKS Digital Scholl</em></p>
@endsection
