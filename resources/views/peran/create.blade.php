@extends('master')

@section('judul')
Halaman Tambah Peran
@endsection

@section('content')

<form action="/cast" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="form-group">
        <label >Nama Peran</label>
        <input type="text" name="nama" class="form-control">
      </div>
      @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label >Cast</label>
      <select name="cast_id" class="form-control" id="">
        <option value="">---Pilih Cast---</option>
        @foreach ($cast as $item)
            <option value="{{$item->id}}">{{$item->nama}}</option>
        @endforeach
      </select>
    </div>

    @error('cast_id')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror

   
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>

  @endsection