@extends('master')

@section('judul')
Halaman Index
@endsection

@section('content')
    <!-- bagian awal, isinya tag heading -->
    <div>
      <h1>Buat Account Baru!</h1>
    </div>

    <div>
      <h2>Sign Up Form</h2>
    </div>
    <!-- form text name -->
    <form action="/welcome" method="post">
      @csrf
      <label>First name</label><br><br>
      <input type="text" name="firstname"><br><br>

      <label>Last name</label><br><br>
      <input type="text" name="lastname"><br><br>
      

      <!-- form radio gender -->
      <p><label>Gender</label></p>
      <input type="radio" name="gender" value="0" />Male<br />
      <input type="radio" name="gender" value="1" />Female<br />
      <input type="radio" name="gender" value="2" />Other

      <!-- form select option nationality -->
      <p><label>Nationality</label></p>
      <select>
        <option value="indonesian">Indonesian</option>
        <option value="english">English</option>
        <option value="arabian">Arabian</option>
      </select>
      <!-- form chechbox gender -->
      <p><label>Language Spoken</label></p>
      <input type="checkbox" name="gender" />Bahasa Indonesia<br />
      <input type="checkbox" name="gender" />English<br />
      <input type="checkbox" name="gender" />Other
      <!-- form text area Bio -->
      <p><label>Bio:</label></p>
      <textarea cols="30" rows="10">Bio...</textarea><br />

      <!-- sign up -->
      <input type="submit" value="signup">
    </form>
    @endsection
 
