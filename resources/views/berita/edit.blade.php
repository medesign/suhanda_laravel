@extends('master')

@section('judul')
Halaman Edit Cast {{$cast->nama}}
@endsection

@section('content')

<form action="/cast/{{$cast->id}}" method="POST">
    @csrf
    @method('PUT')
    <div class="form-group">
        <label >Nama Cast</label>
        <input type="text" name="nama" value="{{$cast->nama}}" class="form-control">
      </div>
      @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
    <div class="form-group">
      <label >Umur</label>
      <input type="integer" name="umur" value="{{$cast->umur}}" class="form-control">
    </div>
    @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
    <div class="form-group">
      <label >Bio</label>
      <textarea name="bio" class="form-control" id="" cols="30" rows="10">{{$cast->bio}}</textarea>
    </div>
    @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
    <div class="form-group form-check">
      <input type="checkbox" class="form-check-input" id="exampleCheck1">
      <label class="form-check-label" for="exampleCheck1">Check me out</label>
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>

  @endsection